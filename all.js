

window.onresize = function(){
	resizeBackground();
};


$(document).ready(function() {
	resizeBackground();
	$("#bg_green").fadeOut(1,function (){})
	$("#bg_blue").fadeOut(1,function (){})
	setTimeout(function(){
		backgroundChainFadeToggle(10000);
	},5000);
	
});

function backgroundChainFadeToggle(delay){
	$("#bg_red").fadeToggle(delay,"linear",function (){});
	$("#bg_green").fadeToggle(delay,"linear",function (){
		$("#bg_green").fadeToggle(delay,"linear",function (){});
		$("#bg_blue").fadeToggle(delay,"linear",function (){
			$("#bg_blue").fadeToggle(delay,"linear",function (){});
			$("#bg_red").fadeToggle(delay,"linear",function (){
				backgroundChainFadeToggle(delay);
			});
		});
	});

}

function resizeBackground(){
	var height = $(document.body)[0].clientHeight;
	$('#bg').attr("style","height:"+height+"px");
}

$('.gamer').mouseover(function (){
    $('.absolute-gamer').fadeIn(500);
});

$('.gamer').mouseleave(function (){
    $('.absolute-gamer').fadeOut(500);
});
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, user-scalable=no initial-scale=1">
	<meta charset="UTF-8">
  	<meta name="description" content="Personal Webpage about Tenebrae /Pavol Lipták/">
  	<meta name="keywords" content="Personal Web,Tenebrae">
  	<meta name="author" content="Tenebrae">
  <title>Tenebrae's Personal Webpage</title>
  	<link rel="stylesheet" type="text/css" href="all.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/bootstrap.min.css">
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-9586691142105417",
    enable_page_level_ads: true
  });
</script>
</head>
<body>
<div id="bg" class="bg_fullscreen">
	<div id="bg_white" class="bg_fullscreen"> </div>
	<div id="bg_red" class="bg_fullscreen"> </div>
	<div id="bg_blue" class="bg_fullscreen"> </div>
	<div id="bg_green" class="bg_fullscreen"> </div>
</div>

<div class="social_media container">
	<div class="col-12">
</div>
<div class="text container">
	<div class="row justify-content-center">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<center><h1>Tenebrae</h1></center>
		</div>
	</div>
	<div class="row">
	  	<div class="col-sm-12">
	  		<div class="row">
	  			<div class="col-sm-2"></div>
	  			<div class="col-sm-4">
	  				<a class="text" target="_blank" href="https://bitbucket.org/Tenebrae00/"><center><h3 class="border button">Developer</h3></center></a>
	  			</div>
	  			<div class="col-sm-4 gamer">
	  				<a class="text button" href="#"><center><h3 class="border button">Gamer</h3></center></a>
	  				<div class="row">
		  				<div class="absolute-gamer border col-sm-12">
		  					<center>
			  					<a target="_blank" href="http://steamcommunity.com/profiles/76561198040012166/games/?tab=recent"><img class="icon" src="img/icon/steam.png"></a>
			  					<a target="_blank" href="https://eu.battle.net/d3/en/profile/Tenebrae-2684/career"><img class="icon" src="img/icon/diablo.png"></a>
			  					<a target="_blank" href="https://worldofwarcraft.com/en-gb/character/drakthul/tenebrew"><img class="icon" src="img/icon/wow.png"></a>
			  					<a target="_blank" href="https://www.twitch.tv/tenebrae00"><img class="icon" src="img/icon/twitch.png"></a>
		  					</center>
		  				</div>
	  				</div>
	  			</div>
	  			<div class="col-sm-2"></div>
	  		</div>
	  	</div>
  	</div>
  	<div class="spacer"></div>
  	<div class="row">
		<div class="col-lg-12 text-content">
			<ul><lightblue>public Class</lightblue> <orange>Tenebrae</orange> <lightblue>extends</lightblue> <orange>Person</orange> <yellow>{</yellow>
				<li>&nbsp;</li>
				<li><lightblue>public string</lightblue> name <red>=</red> "Pavol Lipták";</li>

				<li><lightblue>public</lightblue> <orange>Arraylist</orange><red>&lt;</red><lightblue>string</lightblue><red>&gt;</red> hobbies <red>=</red><br>
					<yellow>[</yellow><yellow>'</yellow>Cycling<yellow>'</yellow><red>,</red>
					<yellow>'</yellow>Hiking<yellow>'</yellow><red>,</red>
					<yellow>'</yellow>Programming<yellow>'</yellow><red>,</red>
					<yellow>'</yellow>LEGO<yellow>'</yellow><red>,</red>
					<yellow>'</yellow>Board&nbsp;Games<yellow>'</yellow><red>,</red>
					<yellow>'</yellow>PC&nbsp;games<yellow>'</yellow><yellow>]</yellow>;
				</li>
				<li>&nbsp;</li>
				<li><a target="_blank" href="https://www.facebook.com/pavol.liptak"><lightblue>public static void</lightblue> <green>showFacebook</green><yellow>()</yellow>;</a></li>
				<li>&nbsp;</li>
				<li><a href="mailto:Tenebrae00@gmail.com"><lightblue>public static void</lightblue> <green>sendEmail</green><yellow>()</yellow>;</a></li>
				<li>&nbsp;</li>
				<li><a target="_blank" href="http://www.blog.tene.sk/"><lightblue>public static void</lightblue> <green>openBlog</green><yellow>()</yellow>;</a></li>
				<li>&nbsp;</li>
			<yellow>}</yellow></ul>
			<div class="spacer"></div>
		</div>
  	</div>
</div>

<div class="socialMediaMenu border" style="padding-top:7px;">
	<center class="nospace">
		<a target="_blank" href="https://www.facebook.com/pavol.liptak"><img class="icon iconSocMedia" src="/img/icon/fb.svg"></a>
		<a target="_blank" href="https://www.linkedin.com/in/pavol-lipt%C3%A1k-aa961b50/"><img class="icon iconSocMedia" src="/img/icon/linkedin.svg"></a>
		<a href="mailto:Tenebrae00@gmail.com"><img class="icon iconSocMedia" src="/img/icon/gmail.png"></a>
		<a target="_blank" href="http://www.blog.tene.sk/"><img class="icon iconSocMedia" src="/img/icon/wp.svg"></a>
	</center>
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-103739766-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
	<script src="bootstrap/jquery-3.2.1.min.js"></script>	
	<script src="bootstrap/bootstrap.min.js"></script>	
	<script src="all.js"></script>	
</html>
